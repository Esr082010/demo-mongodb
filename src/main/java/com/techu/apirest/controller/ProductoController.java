package com.techu.apirest.controller;

import com.techu.apirest.model.ProductoModel;
//import com.techu.apirest.model.ProductoPrecio;
import com.techu.apirest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;


    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();

    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id) {

        return productoService.findById(id);
    }


    @PostMapping("/productos")
    //ejemplo control de find con isPresent
    public boolean postProductos(@RequestBody ProductoModel newProducto) {
        if (productoService.findById(newProducto.getId()).isPresent()) {
            //productoService.save(newProducto);
            return false;
        }
        productoService.save(newProducto);
        return true;

        //productoService.save(newProducto);
        //return newProducto;

    }

    @PutMapping("/productos")
    //Control de find con existById (dado de alta en Producto Service)
    public boolean putProductos(@RequestBody ProductoModel productoToUpdate) {
        if (productoService.existById(productoToUpdate.getId())) {
            productoService.save(productoToUpdate);
            return true;
        } else {
            return false;
        }
    }


    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productoToDelete) {
        return productoService.delete(productoToDelete);
    }

    @DeleteMapping("/productos/{id}")
    public boolean deleteProductoById(@PathVariable String id) {
        if (productoService.existById(id)) {
            productoService.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    //@PatchMapping
    @PatchMapping("/productos/{id}")
    public boolean patchProductos(@RequestBody ProductoModel productoToPatch, @PathVariable String id) {
        if (productoService.existById(id)) {
            productoService.save(productoToPatch);
            return true;
        } else {
            return false;
        }
    }
// Ejemplo con Map, en vez de cogerlo del Json.
    /*
    @PatchMapping("/productos/{id}")
    public ProductoModel patchProductos(@RequestBody Map<String, Object> update, @PathVariable String id) {
        ProductoModel productoToPatch;
        Optional<ProductoModel> oproducto = productoService.findById(id);
        if (oproducto.isPresent()) {
            productoToPatch = oproducto.get();
        } else
            return new ProductoModel("0", "id no existe", "0.00");
        if (update.containsKey("precio"))
            productoToPatch.setPrecio((Double) update.get("precio"));
        if (update.containsKey("descripcion"))
            productoToPatch.setPrecio((String) update.get("descripcion"));

        productoService.save(productoToPatch);
        return productoToPatch;
    }

     */
}