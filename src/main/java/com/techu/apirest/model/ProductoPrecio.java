package com.techu.apirest.model;

public class ProductoPrecio {
    private String id;
    private String precio;

    public void setId(String id) {
        this.id = id;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public String getPrecio() {
        return precio;
    }

    public ProductoPrecio(String id, String precio) {
        this.id = id;
        this.precio = precio;
    }
}
